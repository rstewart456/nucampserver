const express = require('express');
const Favorite = require('../models/favorite');
const cors = require('./cors');

const authenticate = require('../authenticate');
const favoriteRouter = express.Router();

favoriteRouter.route('/')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, authenticate.verifyUser, (req, res, next) => {
    Favorite.findOne({ user: req.user._id })
      .populate('user')
      .populate('campsite')
      // Fixed the issue with campsite modal to allow campsite and user to populate now
      .then(favorite => {
        if (!favorite) {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.send('You have no Favorites')
        } else {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json(favorite)
        }
      })
      .catch(err => next(err))
  })
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorite.findOne({ user: req.user._id })
      .then(favorite => {
        let reqfav = req.body;
        let addfav = []
        if (favorite) {
		reqfav.forEach(addFavor =>  {
			const checkFav = favorite.campsite.includes(addFavor._id)
			if(!checkFav) {
				addfav.push(addFavor._id)
			}
		})
		// This method uses mongoose to add new favorites to database. 
		favorite.update({
			$addToSet: { campsite: req.body}	
          })
            .then(() => {
		if(addfav.length == 0) {
          res.statusCode = 200,
            res.setHeader('Content-Type', 'text/plain');
          res.end('That campsite is already in the list of favorites!')
		} else {
              res.statusCode = 200,
                res.setHeader('Content-Type', 'application/json');
              res.json(addfav)
		}
            })
        } else {
          Favorite.create({ user: req.user._id })
            .then(favorite => {
              reqfav.forEach(favor => {
                favorite.campsite.push(favor._id)
                addfav.push(favor._id)
              })
            }).then(() => {
              res.statusCode = 200,
                res.setHeader('Content-Type', 'application/json');
              res.json(addfav)
            })
            .catch(err => next(err))
        }
      })
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    err = new Error(`The operation isn ot supported`)
    err.status = 403
    return res.end(err)
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorite.findOneAndDelete({ user: req.user._id })
      .then(favorite => {
	// This checks for undefined and tells the user this is no favorites to delete.
	if(favorite == undefined) {
		err = new Error('There are no favorite to delete')
		err.status = 403
		return next(err)
	} else {
        res.statusCode = 200,
          res.setHeader('Content-Type', 'application/json');
        res.json(favorite._id)
	}
      })
      .catch(err => next(err))
  })

favoriteRouter.route('/:favoriteId')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, authenticate.verifyUser, (req, res, next) => {
    err = new Error(`The operation isn ot supported`)
    err.status = 403
    return next(err)
  })
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorite.findOne({ user: req.user._id })
      .then(favorite => {
        let hasFavorite = favorite.campsite.includes(req.params.favoriteId);
        if (!hasFavorite) {
          favorite.campsite.push(req.params.favoriteId)
          favorite.save()
            .then(() => {
              res.statusCode = 200,
                res.setHeader('Content-Type', 'application/json');
              res.json(req.params.favoriteId)
            }).catch(err => next(err))
        } else {
          res.statusCode = 200,
            res.setHeader('Content-Type', 'text/plain');
          res.end('That campsite is already in the list of favorites!')
        }
      })
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    err = new Error(`The operation is not supported`)
    err.status = 403
    return next(err)
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorite.findOne({ user: req.user._id })
      .then(favorite => {
	// checks to see if favorite is there to delete.
        let verifyFav = favorite.campsite.includes(req.params.favoriteId)
        if (verifyFav) {
          let index = favorite.campsite.indexOf(req.params.favoriteId);
          favorite.campsite.splice(index, 1)
          favorite.save()
            .then(() => {
              res.statusCode = 200,
                res.setHeader('Content-Type', 'application/json');
              res.json(req.params.favoriteId)
            }).catch(err => next(err))
        } else {
          res.statusCode = 200,
            res.setHeader('Content-Type', 'text/plain');
          res.end('There are no favorites to delete')
        }
      })
  })

module.exports = favoriteRouter;
