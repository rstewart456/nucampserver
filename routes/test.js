const express = require('express');
const Campsite = require('../models/campsite')
const Favorite = require('../models/favorite')
const testRouter = express.Router();
const cors = require('./cors');
const authenticate = require('../authenticate');

testRouter.route('/')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => {
    Favorite.findOne({ 'users': 'Ghost' })
      .then(favorite => {
      if (favorite) {
        res.send('True')
      } else {
        const nameA = req.body
        const nameB = []
        nameA.forEach(doc => {
          let NameC = doc.name.includes('lee')
          if(NameC) {
            nameB.push(doc)
          }
        })
        res.send(nameB)
      }
      })
  })
//  .post('/', cors.corsWithOptions, (req, res, next) => {
//    Favorite.findOne({ 'users': 'Ghost' })
//      .then(favorite => {
//        if (favorite) {
//          res.send("True")
//        } else {
//          res.send("False")
//        }
//      })
//  })
// testRouter.route('/')
// .get((req, res, next) => {
//   Campsite.findById('613d11d6dac9bf33d61ea821')
//     .then(campsite => {
//       let nameA = ''
//       for(let Key of campsite.comments) {
//         if(Key._id == '613e701095d97c0845c68204')
//           nameA += Key.author
//      }
//       if(nameA == '613b6ce18ce9d518f8046272') {
//         console.log('Is the Author
//       } else {
//         console.log('Not the Author')
//       }
//       console.log(nameA)
//       res.send(nameA)
//     })
// })

//testRouter.route('/')
//  .get((req, res) => {
//    Campsite.findById('613d11d6dac9bf33d61ea821')
//      .then(campsite => {
//        const doc = campsite.comments.filter(word => word._id == '613e701095d97c0845c68204')
//        const docs = Object(doc)
//        const docks = {...doc}
//        console.log(docks.author)
//        res.send(docs)
//      })
//  })

module.exports = testRouter;
